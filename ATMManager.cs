﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace OtusLinq
{
    class ATMManager
    {
        IRepository repository;

        public ATMManager()
        {
            repository = new Repository();
        }


        /// <summary>
        /// Вывод информации о заданном аккаунте по логину и паролю;
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        public void InfoByLoginPassword(string login, string password)
        {
            Console.WriteLine($"Вся информация по пользователю {login}:");
            User user = repository.Users.FirstOrDefault(x => x.Login == login && x.Pasport == password);
            try
            {
                Console.WriteLine($"ID: {user.Id} \nИмя: {user.Name} \nФамилия: {user.LastName} \nОтчество: {user.SecondName} " +
                    $"\nДата Регистрации: {user.RegDate} \nПаспорт: {user.Password}");
            }
            catch(NullReferenceException)
            {
                Console.WriteLine("Не найдено");
            }
        }


        /// <summary>
        /// Вывод данных о всех счетах заданного пользователя
        /// </summary>
        /// <param name="user"></param>
        public void InfoAboutAccount(User user)
        {
            Console.WriteLine($"Данные по счетам пользователя {user.LastName}:");
            repository.Accounts
                .FindAll(x => x.UserId == user.Id)
                .ForEach(x => Console.WriteLine($"Номер счета: {x.Id}"));
        }



        /// <summary>
        /// Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        /// </summary>
        /// <param name="user"></param>
        public void InfoAboutAccountHistory(User user)
        {
            Console.WriteLine($"Данные по счетам и операциям пользователя {user.LastName}:");
            repository.Accounts
                .FindAll(acc => acc.UserId == user.Id)
                .ForEach(acc =>
                {
                    Console.WriteLine($"Номер счета: {acc.Id}");
                    repository.OperationHistories
                    .FindAll(oper => oper.AccountId == acc.Id)
                    .ForEach(oper => Console.WriteLine($"  Операция №:{oper.Id}"));
                });
        }

        /// <summary>
        /// Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
        /// </summary>
        public void InfoIncreaseOperation()
        {
            repository.OperationHistories
                .FindAll(oper => oper.Type == Operation.Increase)
                .ForEach(oper =>
                {
                    Console.WriteLine($"Операция №{oper.Id}, Владелец счета: {oper.Account.User.LastName}");
                    //repository.Users.FirstOrDefault(user=>user.Id ==
                    //repository.Accounts.FirstOrDefault(acc => acc.Id == oper.AccountId).UserId);
                });
        }


        /// <summary>
        /// Вывод данных о всех пользователях у которых на счёте сумма больше N 
        /// </summary>
        /// <param name="border"></param>
        public void InfoSumMoreThen(float border)
        {
            repository.Accounts
                .FindAll(acc => acc.Sum > border)
                .ForEach(acc=>Console.WriteLine($"{acc.User.LastName}"));
        }
    }
}
