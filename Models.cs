﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusLinq
{
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Pasport { get; set; }
        public DateTime RegDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public List<Account> Accounts { get; set; }
    }

    class Account
    {
        public int Id { get; set; }
        public DateTime RegDate { get; set; }
        public float Sum { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        public List <OperationHistory> Histories { get; set; }
    }

    class OperationHistory
    {
        public int Id { get; set; }
        public DateTime RegDate { get; set; }
        public Operation Type { get; set; }
        public float Sum { get; set; }
        public int? AccountId { get; set; }
        public Account Account { get; set; }
    }

    public enum Operation
    {
        Increase,
        Withdraw
    }
}
