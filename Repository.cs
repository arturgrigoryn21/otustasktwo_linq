﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace OtusLinq
{
    interface IRepository
    {
        public List<User> Users { get; set; }
        public List<Account> Accounts { get; set; }
        public List<OperationHistory> OperationHistories { get; set; }

        void Add<T>(T item);
        T GetOne<T>(Func<T, bool> func);
        T GetAll<T>();
    }
    class Repository : IRepository
    {
        List<User> users;
        List<Account> accounts;
        List<OperationHistory> operationHistories;

        #region Propeties
        public List<User> Users
        {
            get
            {
                return users;
            }
            set { users = value; }
        }
        public List<Account> Accounts
        {
            get { return accounts; }
            set { accounts = value; }
        }
        public List<OperationHistory> OperationHistories
        {
            get
            {
                return operationHistories;
            }
            set { operationHistories = value; }
        }
        #endregion

        public Repository()
        {
            users = new List<User>
                {
                    new User{Id=1,Name="Oleg", SecondName="Mihalovish", LastName="Laskov", Password="1232 334322", RegDate=new DateTime(2012,4,2), Login="KROT", Pasport="1234"},
                    new User{Id=2,Name="Dmitriy", SecondName="Olegovich", LastName="Petrov", Password="3432 433332", RegDate=new DateTime(2003,7,22), Login="BLOHA", Pasport="4321"},
                    new User{Id=3,Name="Vasy", SecondName="Pettrovish", LastName="Ivanov", Password="5433 544232", RegDate=new DateTime(2009,12,7), Login="TARAKAN", Pasport="2020"}
                };
            accounts = new List<Account>
                {
                    new Account{Id=4, RegDate =new DateTime(2019,4,5), Sum=350, UserId=1, User=users[0]},
                    new Account{Id=5, RegDate =new DateTime(2018,6,8), Sum=1000, UserId=2, User=users[1]},
                    new Account{Id=6, RegDate =new DateTime(2019,4,6), Sum=200, UserId=1, User=users[0]},
                    new Account{Id=7, RegDate =new DateTime(2020,11,1), Sum=800, UserId=3, User=users[2]}
                };
            operationHistories = new List<OperationHistory>
                {
                    new OperationHistory {Id=8, RegDate=new DateTime(2019,2,3), Sum=50, Type=Operation.Withdraw, AccountId=4, Account=accounts[0]},
                    new OperationHistory {Id=9, RegDate=new DateTime(2019,5,13), Sum=80, Type=Operation.Increase, AccountId=6, Account=accounts[2]},
                    new OperationHistory {Id=10, RegDate=new DateTime(2019,5,17), Sum=180, Type=Operation.Withdraw, AccountId=5, Account=accounts[1]},
                    new OperationHistory {Id=11, RegDate=new DateTime(2019,6,25), Sum=100, Type=Operation.Increase, AccountId=4, Account=accounts[0]},
                    new OperationHistory {Id=12, RegDate=new DateTime(2019,5,18), Sum=30, Type=Operation.Withdraw, AccountId=5, Account=accounts[1]},
                };
            
        }

        public void Add<T>(T item)
        {
            throw new NotImplementedException();
        }

        public T GetAll<T>()
        {
            throw new NotImplementedException();
        }

        public T GetOne<T>(Func<T, bool> func)
        {
            throw new NotImplementedException();
        }
    }
}
